package de.naturzukunft.fairsync.organisationadapter.oauth.ui;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;

import de.naturzukunft.rdf.solid.organisation.adapter.Organisation;

public class OrganisationForm extends FormLayout {

	TextField name = new TextField("Name");

	TextField note = new TextField("Note");
	EmailField unlabeledEmail = new EmailField("unlabeledEmail");
	ComboBox<Organisation> organisation = new ComboBox<>("Organisation");

	Button save = new Button("Save");
	Button delete = new Button("Delete");
	Button close = new Button("Cancel");

	Binder<Organisation> binder = new BeanValidationBinder<>(Organisation.class);

	public OrganisationForm() {
		addClassName("OrganisationForm");
		binder.bindInstanceFields(this);

		add(name, note, unlabeledEmail, createButtonsLayout());
	}

	public void setOrganisation(Organisation organisation) {
		binder.setBean(organisation);
	}

	private HorizontalLayout createButtonsLayout() {
		save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
		close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		save.addClickShortcut(Key.ENTER);
		close.addClickShortcut(Key.ESCAPE);
		save.addClickListener(click -> validateAndSave());
		delete.addClickListener(click -> fireEvent(new DeleteEvent(this,  binder.getBean())));
		close.addClickListener(click -> fireEvent(new CloseEvent(this)));
		
		binder.addStatusChangeListener(evt -> save.setEnabled(binder.isValid()));
		
		return new HorizontalLayout(save, delete, close);
	}

	private Object validateAndSave() {
		if(binder.isValid()) {
			fireEvent(new SaveEvent(this, binder.getBean()));
		}
		return null;
	}

	// Events
    public static abstract class OrganisationFormEvent extends ComponentEvent<OrganisationForm> {
      private Organisation organisation;

      protected OrganisationFormEvent(OrganisationForm source, Organisation organisation) {
        super(source, false);
        this.organisation = organisation;
      }

      public Organisation getOrganisation() {
        return organisation;
      }
    }

    public static class SaveEvent extends OrganisationFormEvent {
      SaveEvent(OrganisationForm source, Organisation organisation) {
        super(source, organisation);
      }
    }

    public static class DeleteEvent extends OrganisationFormEvent {
      DeleteEvent(OrganisationForm source, Organisation organisation) {
        super(source, organisation);
      }

    }

    public static class CloseEvent extends OrganisationFormEvent {
      CloseEvent(OrganisationForm source) {
        super(source, null);
      }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
      return getEventBus().addListener(eventType, listener);
    }
}
