
package de.naturzukunft.fairsync.organisationadapter.oauth.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;

import de.naturzukunft.fairsync.organisationadapter.oauth.data.UserSession;
import de.naturzukunft.fairsync.organisationadapter.oauth.ui.OrganisationForm.CloseEvent;
import de.naturzukunft.fairsync.organisationadapter.oauth.ui.OrganisationForm.DeleteEvent;
import de.naturzukunft.fairsync.organisationadapter.oauth.ui.OrganisationForm.SaveEvent;
import de.naturzukunft.rdf.solid.organisation.adapter.AuthInfo;
import de.naturzukunft.rdf.solid.organisation.adapter.AuthInfoData;
import de.naturzukunft.rdf.solid.organisation.adapter.Organisation;
import de.naturzukunft.rdf.solid.organisation.adapter.OrganisationsRepository;
import de.naturzukunft.rdf.solid.organisation.adapter.PodStorageFactory;
import de.naturzukunft.rdf.solid.organisation.adapter.Storage;
import lombok.extern.slf4j.Slf4j;

/**
 * Application main class that is hidden to user before authentication.
 */
@Route("")
@CssImport("./styles/shared-styles.css")
@Slf4j
public class MainView extends VerticalLayout {

    @Autowired
    UserSession userSession;

	private static final long serialVersionUID = 6385958548455451493L;	
	private OrganisationsRepository organisationsRepository;
	private AuthInfo authInfo;
	private PodStorageFactory podStorageFactory; 
	
	private Grid<Organisation> grid = new Grid<>(Organisation.class);
	private TextField filterText = new TextField();

	private OrganisationForm form;
	
    public MainView(OrganisationsRepository organisationsRepository, AuthInfo authInfo, PodStorageFactory podStorageFactory ) {
    	 this.organisationsRepository = organisationsRepository;
		this.authInfo = authInfo;
		this.podStorageFactory = podStorageFactory;
		addClassName("list-view"); 
         setSizeFull(); 
         configureGrid();
         
         form = new OrganisationForm();
         form.addListener(SaveEvent.class, this::saveOrganisation);
         form.addListener(DeleteEvent.class, this::deleteOrganisation);
         form.addListener(CloseEvent.class, evt -> closeEditor());
         Div content = new Div(grid,form);
         content.addClassName("content");
         content.setSizeFull();
         
         add(getToolbar(), content );
         updateOrganisations();
         closeEditor();
    }

    private void deleteOrganisation(DeleteEvent deleteEvent) {
		Optional<AuthInfoData> authInfoDataOptional = authInfo.getAuthInfoData();
		if(authInfoDataOptional.isPresent()) {
			AuthInfoData authInfoData = authInfoDataOptional.get();
	    	Storage storage = podStorageFactory.getStorage(authInfoData);
	    	organisationsRepository.delete(storage, authInfoData.getWebId(), deleteEvent.getOrganisation());
	    	updateOrganisations();
	    	closeEditor();
		} else {
			log.warn("todo errorhandling");
		}
    }
    
    private void saveOrganisation(SaveEvent saveEvent) {
    	
		Optional<AuthInfoData> authInfoDataOptional = authInfo.getAuthInfoData();
		if(authInfoDataOptional.isPresent()) {
			AuthInfoData authInfoData = authInfoDataOptional.get();
	    	Storage storage = podStorageFactory.getStorage(authInfoData);
	    	organisationsRepository.save(storage, authInfoData.getWebId(), saveEvent.getOrganisation());
	    	updateOrganisations();
	    	closeEditor();
		} else {
			log.warn("todo errorhandling");
		}
    }
    
    private void closeEditor() {
		form.setOrganisation(null);
		form.setVisible(false);
		removeClassName("editing");
	}
 
	private HorizontalLayout getToolbar() {
		filterText.setPlaceholder("filter by name");
		filterText.setClearButtonVisible(true);
		filterText.setValueChangeMode(ValueChangeMode.LAZY);
		filterText.addValueChangeListener(e -> updateOrganisations());
		
		grid.asSingleSelect().addValueChangeListener( evt -> editOrganisation(evt.getValue()));
		
		Button addOrganisation = new Button("Add organisation", click -> addOrganisation());
		HorizontalLayout toolbar = new HorizontalLayout(filterText, addOrganisation); 
		toolbar.addClassName("toolbar");
		return toolbar;		
	}

	private void addOrganisation() {
		grid.asSingleSelect().clear();
		editOrganisation(new Organisation());
	}

	private void editOrganisation(Organisation organisation) {
		if(organisation == null) {
			closeEditor();
		} else {
			 form.setOrganisation(organisation);
			 form.setVisible(true);
			 addClassName("editing");
		}
	}

	private void updateOrganisations() {
		Optional<AuthInfoData> authInfoDataOptional = authInfo.getAuthInfoData();
		if(authInfoDataOptional.isPresent()) {
			AuthInfoData authInfoData = authInfoDataOptional.get();
	    	Storage storage = podStorageFactory.getStorage(authInfoData);
	    	String filtered = filterText.getValue();
	    	List<Organisation> organisations = new ArrayList<>();
			if(StringUtils.hasText(filtered)) {
				organisations = organisationsRepository.findByName(storage, authInfoData.getWebId(), filtered);
			} else {
				organisations = organisationsRepository.findAll(storage, authInfoData.getWebId());
			}
	    	grid.setItems(organisations);
		}
	}


	private void configureGrid() {
        grid.addClassName("organisation-grid");
        grid.setSizeFull();
        grid.setColumns("name", "note", "unlabeledEmail", "homepage"); 
    }
}
